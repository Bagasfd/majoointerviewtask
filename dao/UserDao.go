package dao

import (
	"MajooTaskInterview/repository"
	"database/sql"
	"errors"
)

type userDao struct {
	TableName string
}

var UserDao = userDao{}.New()

func (input userDao) New() (output userDao) {
	output.TableName = "users"
	return
}

func (input userDao) GetUsersByUsername(db *sql.DB, userParam repository.UsersModel)(result repository.UsersModel, err error) {
	query := "SELECT id, user_name, password FROM " +
		input.TableName + " WHERE user_name = $1 "

	param := []interface{}{userParam.Username.String}

	err = db.QueryRow(query, param...).Scan(&result.Id, &result.Username, &result.Password)
	if err != nil && err.Error() != "sql: no rows in result set" {
		err = errors.New("maaf ada yang salah di system kami")
		return
	}

	err = nil
	return
}