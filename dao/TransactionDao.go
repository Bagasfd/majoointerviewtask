package dao

import (
	"MajooTaskInterview/repository"
	"database/sql"
	"errors"
)

type transactionDao struct {
	TableName string
}

var TransactionDao = transactionDao{}.New()

func (input transactionDao) New() (output transactionDao) {
	output.TableName = "transactions"
	return
}

func (input transactionDao) GetTransactionsReportByMerchant(db *sql.DB, userParam repository.UsersModel, page int, limit int) (result []repository.ReportMerchantMontlyModel, err error) {
	query :=
		"SELECT " +
			"   m.merchant_name as merchant_name, sum(trns.bill_total) as omzet , to_char(trns.created_at, 'YYYY-MM-DD')  as days " +
			" FROM " +
			input.TableName + " as trns " +
			"LEFT JOIN merchants m ON m.id  = trns.merchants_id " +
			"WHERE trns.created_at between '2021-11-01' AND '2021-11-30' AND m.user_id = $1 " +
			"group by m.merchant_name, days " +
			"LIMIT $2 OFFSET $3 "

	queryParam := []interface{}{userParam.Id.Int64, limit, (page - 1) * limit}

	rows, errorS := db.Query(query, queryParam...)
	if errorS != nil && errorS.Error() != "sql: no rows in result set" {
		err = errors.New("maaf system sedang bermasalah")
		return
	}

	for rows.Next() {
		var each = repository.ReportMerchantMontlyModel{}
		var errorS = rows.Scan(&each.MerchantName, &each.Omzet, &each.Days)
		if errorS != nil {
			err = errors.New("maaf system sedang bermasalah")
			return
		}
		result = append(result, each)
	}

	err = nil
	return
}

func (input transactionDao) GetTransactionsReportByOutlet(db *sql.DB, userParam repository.UsersModel, page int, limit int) (result []repository.ReportOutletMontlyModel, err error) {
	query :=
		"SELECT " +
			"   m.merchant_name as merchant_name,  o.outlet_name, sum(trns.bill_total) as omzet , to_char(trns.created_at, 'YYYY-MM-DD')  as days " +
			" FROM " +
			input.TableName + " as trns " +
			"LEFT JOIN merchants m ON m.id  = trns.merchants_id " +
			"LEFT JOIN outlets  o ON o.id  = trns.outlet_id  and o.merchants_id  = m.id " +
			"WHERE trns.created_at between '2021-11-01' AND '2021-11-30' AND m.user_id = $1 " +
			"group by m.merchant_name, o.outlet_name, days " +
			"LIMIT $2 OFFSET $3 "

	queryParam := []interface{}{userParam.Id.Int64, limit, (page - 1) * limit}

	rows, errorS := db.Query(query, queryParam...)
	if errorS != nil && errorS.Error() != "sql: no rows in result set" {
		err = errors.New("maaf system sedang bermasalah")
		return
	}

	for rows.Next() {
		var each = repository.ReportOutletMontlyModel{}
		var errorS = rows.Scan(&each.MerchantName, &each.OutletName, &each.Omzet, &each.Days)
		if errorS != nil {
			err = errors.New("maaf system sedang bermasalah")
			return
		}
		result = append(result, each)
	}

	err = nil
	return
}