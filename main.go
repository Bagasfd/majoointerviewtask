package main

import (
	"MajooTaskInterview/config"
	"MajooTaskInterview/router"
	"github.com/gobuffalo/packr/v2"
	migrate "github.com/rubenv/sql-migrate"
	"log"
)

func main() {
	config.GenerateConfiguration("./config")
	config.SetServerAttribute()

	dbMigrate()

	router.APIController()
}


func dbMigrate() {
	migrations := migrate.PackrMigrationSource{
		Box: packr.New("migrations", "./sql_migration"),
	}
	if config.ServerAttribute.DBConnection != nil {
		n, err := migrate.Exec(config.ServerAttribute.DBConnection, "postgres", migrations, migrate.Up)
		if err != nil {
			log.Fatal("Error on migrate migration")
		} else {
			log.Println("Success Migrate migration", n)
		}
	} else {
		log.Println("Error Migrate migration database null")
	}
}
