module MajooTaskInterview

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/ghodss/yaml v1.0.0 // indirect
	github.com/gobuffalo/packr/v2 v2.8.3
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rubenv/sql-migrate v1.1.2
	github.com/tkanos/gonfig v0.0.0-20210106201359-53e13348de2f
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
