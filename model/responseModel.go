package model

import "MajooTaskInterview/util"

type ResponseModel struct {
	Status  bool        `json:"status"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func (input ResponseModel) String() string {
	return util.StructToJSON(input)
}
