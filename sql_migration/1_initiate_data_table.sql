-- +migrate Up

-- +migrate StatementBegin

CREATE SEQUENCE IF NOT EXISTS users_pkey_seq;
CREATE TABLE "users"
(
    id         BIGINT NOT NULL             DEFAULT nextval('users_pkey_seq'::regclass),
    user_name  VARCHAR(45),
    password   VARCHAR(255),
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    created_by BIGINT,
    updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_by BIGINT,
    CONSTRAINT users_pkey PRIMARY KEY (id)
);

CREATE SEQUENCE IF NOT EXISTS merchants_pkey_seq;
CREATE TABLE "merchants"
(
    id            BIGINT NOT NULL             DEFAULT nextval('merchants_pkey_seq'::regclass),
    user_id       BIGINT,
    merchant_name VARCHAR(256),
    created_at    TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    created_by    BIGINT,
    updated_at    TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_by    BIGINT,
    CONSTRAINT merchants_pkey PRIMARY KEY (id),
    CONSTRAINT fk_merchants_user_id FOREIGN KEY (user_id) REFERENCES users (id)

);


CREATE SEQUENCE IF NOT EXISTS outlets_pkey_seq;
CREATE TABLE "outlets"
(
    id           BIGINT NOT NULL             DEFAULT nextval('outlets_pkey_seq'::regclass),
    merchants_id BIGINT,
    outlet_name  VARCHAR(256),
    created_at   TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    created_by   BIGINT,
    updated_at   TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_by   BIGINT,
    CONSTRAINT outlets_pkey PRIMARY KEY (id),
    CONSTRAINT fk_outlets_merchants_id FOREIGN KEY (merchants_id) REFERENCES merchants (id)

);

CREATE SEQUENCE IF NOT EXISTS transactions_pkey_seq;
CREATE TABLE "transactions"
(
    id           BIGINT NOT NULL             DEFAULT nextval('transactions_pkey_seq'::regclass),
    merchants_id BIGINT,
    outlet_id    BIGINT,
    bill_total   float8,
    created_at   TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    created_by   BIGINT,
    updated_at   TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    updated_by   BIGINT,
    CONSTRAINT transactions_pkey PRIMARY KEY (id),
    CONSTRAINT fk_transactions_merchants_id FOREIGN KEY (merchants_id) REFERENCES merchants (id),
    CONSTRAINT fk_transactions_outlets_id FOREIGN KEY (outlet_id) REFERENCES outlets (id)
);


INSERT INTO users (user_name, password, created_by, created_at, updated_by, updated_at)
VALUES ('Admin 1', 'admin1', 0, CURRENT_TIMESTAMP, 0, CURRENT_TIMESTAMP),
       ('Admin 2', 'admin2', 0, CURRENT_TIMESTAMP, 0, CURRENT_TIMESTAMP);

INSERT INTO merchants (id, user_id, merchant_name, created_at, created_by, updated_at, updated_by)
VALUES (1, 1, 'merchant 1', CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 2, 'Merchant 2', CURRENT_TIMESTAMP, 2, CURRENT_TIMESTAMP, 2);

INSERT INTO outlets (id, merchants_id, outlet_name, created_at, created_by, updated_at, updated_by)
VALUES (1, 1, 'Outlet 1', CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1),
       (2, 2, 'Outlet 1', CURRENT_TIMESTAMP, 2, CURRENT_TIMESTAMP, 2),
       (3, 1, 'Outlet 2', CURRENT_TIMESTAMP, 1, CURRENT_TIMESTAMP, 1);


INSERT INTO transactions (id, merchants_id, outlet_id, bill_total, created_at, created_by, updated_at, updated_by)
VALUES (1, 1, 1, 2000, '2021-11-01 12:30:04', 1, '2021-11-01 12:30:04', 1),
       (2, 1, 1, 2500, '2021-11-01 17:20:14', 1, '2021-11-01 17:20:14', 1),
       (3, 1, 1, 4000, '2021-11-02 12:30:04', 1, '2021-11-02 12:30:04', 1),
       (4, 1, 1, 1000, '2021-11-04 12:30:04', 1, '2021-11-04 12:30:04', 1),
       (5, 1, 1, 7000, '2021-11-05 16:59:30', 1, '2021-11-05 16:59:30', 1),
       (6, 1, 3, 2000, '2021-11-02 18:30:04', 1, '2021-11-02 18:30:04', 1),
       (7, 1, 3, 2500, '2021-11-03 17:20:14', 1, '2021-11-03 17:20:14', 1),
       (8, 1, 3, 4000, '2021-11-04 12:30:04', 1, '2021-11-04 12:30:04', 1),
       (9, 1, 3, 1000, '2021-11-04 12:31:04', 1, '2021-11-04 12:31:04', 1),
       (10, 1, 3, 7000, '2021-11-05 16:59:30', 1, '2021-11-05 16:59:30', 1),
       (11, 2, 2, 2000, '2021-11-01 18:30:04', 2, '2021-11-01 18:30:04', 2),
       (12, 2, 2, 2500, '2021-11-02 17:20:14', 2, '2021-11-02 17:20:14', 2),
       (13, 2, 2, 4000, '2021-11-03 12:30:04', 2, '2021-11-03 12:30:04', 2),
       (14, 2, 2, 1000, '2021-11-04 12:31:04', 2, '2021-11-04 12:31:04', 2),
       (15, 2, 2, 7000, '2021-11-05 16:59:30', 2, '2021-11-05 16:59:30', 2),
       (16, 2, 2, 2000, '2021-11-05 18:30:04', 2, '2021-11-05 18:30:04', 2),
       (17, 2, 2, 2500, '2021-11-06 17:20:14', 2, '2021-11-06 17:20:14', 2),
       (18, 2, 2, 4000, '2021-11-07 12:30:04', 2, '2021-11-07 12:30:04', 2),
       (19, 2, 2, 1000, '2021-11-08 12:31:04', 2, '2021-11-08 12:31:04', 2),
       (20, 2, 2, 7000, '2021-11-09 16:59:30', 2, '2021-11-09 16:59:30', 2),
       (21, 2, 2, 1000, '2021-11-10 12:31:04', 2, '2021-11-10 12:31:04', 2),
       (22, 2, 2, 7000, '2021-11-11 16:59:30', 2, '2021-11-11 16:59:30', 2);

-- +migrate StatementEnd
