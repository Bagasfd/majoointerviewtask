package util

import (
	"MajooTaskInterview/config"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"strings"
	"time"
)


type JWTToken struct {
}

func (input JWTToken) generateJWT(Payload jwt.Claims, key string) (string, error) {
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS512, Payload)
	token, err := jwtToken.SignedString([]byte(key))
	if err != nil {
		return "", err
	}
	return token, nil
}

func (input JWTToken) GenerateToken(Payload jwt.Claims, key string) (string, error) {
	return input.generateJWT(Payload, key)
}

type PayloadJWT struct {
	Username string
	jwt.StandardClaims
}

func GenerateInternalToken(username string) (result map[string]string) {
	result = make(map[string]string)
	tokenCode := PayloadJWT{
		Username: username,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 * time.Hour).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}

	jwtToken, _ := JWTToken{}.GenerateToken(tokenCode, config.ApplicationConfiguration.GetJWTKey())
	result["token"] = jwtToken
	return
}


func ValidateJWTInternal(jwtTokenStr string, key string) (payload PayloadJWT, err error) {
	claims := &PayloadJWT{}

	jwtToken, err := jwt.ParseWithClaims(jwtTokenStr, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})

	if err != nil {
		if strings.Contains(err.Error(), "expired") {
			err = errors.New("token sudah kardaluasa")
		}
		err = errors.New("token tidak valid")
		return
	}

	if jwtToken.Header["alg"] != "HS512" && jwtToken.Header["alg"] != "HS256" {
		return payload, errors.New("token tidak valid")
	}

	payload = *jwtToken.Claims.(*PayloadJWT)
	return
}
