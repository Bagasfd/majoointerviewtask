package util

import (
	"errors"
	"io/ioutil"
	"net/http"
)

func ReadBodyService(request *http.Request) (string, error) {
	var stringBody string
	var err error

	if request.Method != "GET" {
		stringBody, _, err = readBody(request)
		if err != nil {
			return "", err
		}

	}

	return stringBody, nil
}

func readBody(request *http.Request) (output string, bodySize int, err error) {
	byteBody, err := ioutil.ReadAll(request.Body)
	defer request.Body.Close()
	if err != nil {
		return "", 0, errors.New("BODY_INVALID")
	}
	return string(byteBody), len(byteBody), nil
}
