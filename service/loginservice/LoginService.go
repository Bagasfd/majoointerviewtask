package loginservice

import (
	"MajooTaskInterview/config"
	"MajooTaskInterview/dao"
	"MajooTaskInterview/model"
	"MajooTaskInterview/repository"
	"MajooTaskInterview/service"
	"MajooTaskInterview/util"
	"database/sql"
	"errors"
	"net/http"
)

func UserLogin (response http.ResponseWriter, request *http.Request) {
	var err error
	var responseService map[string]string
	var responseModel model.ResponseModel

	defer func() {
		if err != nil {
			responseModel = model.ResponseModel{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			}
		}else {
			responseModel = model.ResponseModel{
				Status:  true,
				Message: "berhasil login user",
				Data:    responseService,
			}
		}
		response.Header().Add("Content-Type", "application/json")
		response.Write([]byte(responseModel.String()))
	}()

	inputStruct, err := service.ReadBodyAndValidateService(request)
	if err != nil {
		return
	}

	db := config.ServerAttribute.DBConnection

	userOnDB, err := dao.UserDao.GetUsersByUsername(db, repository.UsersModel{Username: sql.NullString{String: inputStruct.Username}})
	if err != nil {
		return
	}

	if userOnDB.Id.Int64 == 0 {
		err = errors.New("user belum terdaftar")
		return
	}

	if userOnDB.Password.String !=inputStruct.Password {
		err = errors.New("username atau password tidak cocok")
		return
	}

	responseService = util.GenerateInternalToken(inputStruct.Username)
	return
}