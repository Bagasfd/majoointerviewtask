package merchantservice

import (
	"MajooTaskInterview/config"
	"MajooTaskInterview/dao"
	"MajooTaskInterview/model"
	"MajooTaskInterview/repository"
	"MajooTaskInterview/service"
	"MajooTaskInterview/util"
	"database/sql"
	"net/http"
)

func ReportMonthlyMerchantOmzet (response http.ResponseWriter, request *http.Request) {
	var err error
	var responseService interface{}
	var responseModel model.ResponseModel

	defer func() {
		if err != nil {
			responseModel = model.ResponseModel{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			}
		}else {
			responseModel = model.ResponseModel{
				Status:  true,
				Message: "berhasil ambil data laporan",
				Data:    responseService,
			}
		}
		response.Header().Add("Content-Type", "application/json")
		response.Write([]byte(responseModel.String()))
	}()

	payLoadJwt, err := util.ValidateJWTInternal(request.Header.Get("Authorization"), config.ApplicationConfiguration.GetJWTKey())
	if err != nil {
		return
	}

	inputStruct := service.ReadGetListDataService(request)

	db := config.ServerAttribute.DBConnection

	userOnDb, err := dao.UserDao.GetUsersByUsername(db, repository.UsersModel{Username: sql.NullString{String: payLoadJwt.Username}})
	if err != nil {
		return
	}

	responseService, err = dao.TransactionDao.GetTransactionsReportByMerchant(db, repository.UsersModel{Id: userOnDb.Id}, inputStruct.Page, inputStruct.Limit)
	if err != nil {
		return
	}

	err = nil
	return
}