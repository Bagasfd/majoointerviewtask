package service

import (
	"MajooTaskInterview/dto"
	"MajooTaskInterview/util"
	"encoding/json"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func ReadBodyAndValidateService(request *http.Request) (inputStruct dto.RequestUserLogin, err error) {
	var stringBody string

	stringBody, err = util.ReadBodyService(request)
	if err != nil {
		return
	}

	_ = json.Unmarshal([]byte(stringBody), &inputStruct)

	err = inputStruct.ValidateLoginUser()
	return
}


func ReadGetListDataService (request *http.Request) (inputStruct dto.GetListDataRequest) {
	queryParam := generateQueryParam(request)

	inputStruct.Page, _ = strconv.Atoi(queryParam["page"])
	inputStruct.Limit, _ = strconv.Atoi(queryParam["limit"])
	inputStruct.Filter = queryParam["filter"]
	return
}

func generateQueryParam(request *http.Request) map[string]string {
	result := make(map[string]string)
	defer func() {
		_ = recover()
	}()

	var errs error

	rawQuery := request.URL.RawQuery

	rawSplit := strings.Split(rawQuery, "&")
	for key := range rawSplit {
		splitEqual := strings.Split(rawSplit[key], "=")
		result[splitEqual[0]], errs = url.QueryUnescape(splitEqual[1])
		if errs != nil {
			result[splitEqual[0]] = splitEqual[1]
		}
	}

	return result
}
