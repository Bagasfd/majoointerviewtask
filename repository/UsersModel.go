package repository

import "database/sql"

type UsersModel struct {
	Id        sql.NullInt64
	Username  sql.NullString
	Password  sql.NullString
	CreatedBy sql.NullInt64
	CreatedAt sql.NullTime
	UpdatedBy sql.NullInt64
	UpdatedAt sql.NullTime
}
