package repository

type ReportMerchantMontlyModel struct {
	MerchantName string  `json:"merchant_name"`
	Omzet        float64 `json:"omzet"`
	Days         string  `json:"days"`
}

type ReportOutletMontlyModel struct {
	MerchantName string  `json:"merchant_name"`
	OutletName   string  `json:"outlet_name"`
	Omzet        float64 `json:"omzet"`
	Days         string  `json:"days"`
}
