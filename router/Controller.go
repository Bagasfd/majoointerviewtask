package router

import (
	"MajooTaskInterview/config"
	"MajooTaskInterview/service/loginservice"
	"MajooTaskInterview/service/merchantservice"
	"MajooTaskInterview/service/outletservice"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

func APIController() {
	handler := mux.NewRouter()

	handler.HandleFunc("/login", loginservice.UserLogin).Methods("POST")
	handler.HandleFunc("/report-omzet-merchant", merchantservice.ReportMonthlyMerchantOmzet)
	handler.HandleFunc("/report-omzet-outlet", outletservice.ReportMonthlyOutletOmzet)



	fmt.Println(http.ListenAndServe(config.ApplicationConfiguration.GetServerHost()+":"+config.ApplicationConfiguration.GetServerPort(), handler))
}
