# Majoo Interview Task

## Get Started
pada file config-development.json update beberapa configuration sesuaikan dengan settingan local
```
//Configuration Setup
{
  "server" : {
    "host" : "localhost",
    "port" : "8080",
    "service_id" : "delivery"
  },
  "postgresql": {
    "address": "user=username password=pass dbname=dbname sslmode=disable host=localhost port=5432",
    "default_schema": "majoo-task",
    "max_open_connection": 500,
    "max_idle_connection": 100
  },
  "jwt" : {
    "key" : "test"
  }
}
```

# Soal Pertama 

## A.Membuat Fungsi Login 

```
URL : http://localhost:8082/login
Request : 
{
    "username" : "Admin 1",
    "password" : "admin1"
}
Response : 
{
    "status": true,
    "message": "berhasil login user",
    "data": {
        "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI
        6IkFkbWluIDEiLCJleHAiOjE2NTgwNDgzODMsImlhdCI6MTY1Nzk2MTk4M30.sx
        dsQFZCf5skSz1xwdGxK_1JfKoMC_Gc0qHFx4yxjGg1gUQkwiHDLeyKQZN4FeRixVSUulkNZEWUeNeLYq26NQ"
    }
}
```

Fungsi login ini akan membalikan status success dan gagal bila users salah memasukan kombinasi password dan username

```
URL : http://localhost:8082/login
Request : 
{
    "username" : "Admin 1",
    "password" : "asal-asalan"
}
Response : 
{
    "status": false,
    "message": "username atau password tidak cocok",
    "data": null
}
```
## B.Untuk authorization pada point A gunakan JWT
```
{
   // field token berisi jwt token yang diperlukan 
   // untuk melakukan hit api lain  
   "status": true,
   "message": "berhasil login user",
   "data": {
      "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJVc2VybmFtZSI
      6IkFkbWluIDEiLCJleHAiOjE2NTgwNDgzODMsImlhdCI6MTY1Nzk2MTk4M30.sx
      dsQFZCf5skSz1xwdGxK_1JfKoMC_Gc0qHFx4yxjGg1gUQkwiHDLeyKQZN4FeRixVSUulkNZEWUeNeLYq26NQ"
   }
}
```
![alt text](https://ik.imagekit.io/f0f706sv1/Screenshot_46_Kg3_P2OU0.png?ik-sdk-version=javascript-1.4.3&updatedAt=1657974116098)
Jika user tidak menggunakan token atau memasukan token yang salah maka system akan melakukan validasi dan menampilkan pesan error

## C.Laporan nama merchant, omzet per hari dalam pada bulan november
```
{
   URL GET : http://localhost:8080/report-omzet-merchant?page=1&limit=2
   PARAMS : PAGE = 1, LIMIT 2

   RESPONSE : 
   {
       "status": true,
       "message": "berhasil ambil data laporan",
       "data": [
           {
               "merchant_name": "merchant 1",
               "omzet": 4500,
               "days": "2021-11-01"
           },
           {
               "merchant_name": "merchant 1",
               "omzet": 6000,
               "days": "2021-11-02"
           }
       ]
   }
}
```
Dapat dilihat bahwa data report sudah dapat di tarik dan melakukan grouping berdasarkan merchant dan omzet perhari merchant
data sudah di lakukan paging dan limit

## D.API menampilkan laporan nama merchant, nama outlet, omzet per hari pada bulan november
```
 {
    URL GET : http://localhost:8080/report-omzet-outlet?page=1&limit=2
    PARAMS : PAGE = 1, LIMIT 2
 
    RESPONSE : 
    {
        "status": true,
        "message": "berhasil ambil data laporan",
        "data": [
            {
                "merchant_name": "merchant 1",
                "outlet_name": "Outlet 1",
                "omzet": 4500,
                "days": "2021-11-01"
            },
            {
                "merchant_name": "merchant 1",
                "outlet_name": "Outlet 1",
                "omzet": 4000,
                "days": "2021-11-02"
            }
        ]
    }
 }
 ```
Dapat dilihat bahwa data report sudah dapat di tarik dan melakukan grouping berdasarkan merchant, outlet dan omzet perhari merchant
data sudah di lakukan paging dan limit

## E. Pada poin C pastikan user tidak bisa melakukan akses pada merchant_id yang bukan miliknya
Saat user melakukan login maka akan mendapatkan jwt token token ini akan di gunakan untuk melakukan grouping dan handle data group
agar merchant yang di tarik hanya merchant yang di miliki user
```
  "data": [
              {
                  "merchant_name": "merchant 1",
                  "outlet_name": "Outlet 1",
                  "omzet": 4500,
                  "days": "2021-11-01"
              },
              {
                  "merchant_name": "merchant 1",
                  "outlet_name": "Outlet 1",
                  "omzet": 4000,
                  "days": "2021-11-02"
              }
          ]
     //yang di tarik hanya merchant1 ini karena login menggunakan user Admin1
    //dimana admin1 adalah pemiliki merchant 1 
    
 ```

## F. Pada poin D pastikan user tidak bisa melakukan akses pada merchant_id, outlet_id yang bukan miliknya
Saat user melakukan login maka akan mendapatkan jwt token token ini akan di gunakan untuk melakukan grouping dan handle data group
agar merchant yang di tarik hanya merchant yang di miliki user
```
  "data": [
         {
             "merchant_name": "merchant 1",
             "omzet": 4500,
             "days": "2021-11-01"
         },
         {
             "merchant_name": "merchant 1",
             "omzet": 4000,
             "days": "2021-11-02"
         }
     ]
     //yang di tarik hanya merchant1 ini karena login menggunakan user Admin1
    //dimana admin1 adalah pemiliki merchant 1 dan outlet 1
    
 ```

## G. Dari test case pada point C dan point D, apakah struktur ERD yang dibentuk sudah optimal? berikan penjelasannya
Menurut saya mungkin bisa di perbaiki lagi agar report bisa di generate lebih cepat mungkin perlu mengurangi 
beberapa fungsi join untuk mengambil nama merchant dan outlet dengan cara membuat table report sendiri dan menyesuaikan 
field table report dengan kebutuhan report nya dengan cara begini untuk report dengan akumulasi data yang besar harusnya 
bisa lebih baik dan cepat


## H. Dokumen teknis Data Manipulation Language (DML)
Data manipulation language adalah salah satu bahasa manipulasi yang di miliki sql
beberapa dml adalah
UPDATE untuk melakukan update pada data
INSERT untuk menambah data
DELETE untuk menghapus data


# Soal Kedua
```
   A.Struct Parrent Method InsertArea saat di panggil salah harusnya menggunakan AreaRepository bukan_u.repository 
    Contoh : 
	areaRepo := AreaRepository{}
	err = areaRepo.InsertArea(params....)
	
   B.Pada saat mereturn error log di jalankan terlebih dahulu sebelum error baru di berikan ini akan mengakibatkan error pada log belum terisi dengan
    nilai yang ingin di asaign pada errors.New(...)
    Contoh : 
	if err != nil {
		//kode ini akan membua log error yang di tampilkan sesuai dengan yang di isi
		err = errors.New(en.ERROR_DATABASE)
		log.Error().Msg(err.Error())
		return err
	}
	
	
   C.Parameter yang di masukan oleh developer kedalam function salah 
	Contoh : 
	function memiliki parameter : param1 int32, param2 int64,  type []string, ar *Model.Area
	tetapi developer memasukan  : 10          , 10          ,  'persegi'
	
	dapat di lihat pada contoh di atas user memasukan parameter yang kurang bahkan salah
	
	Penggunaan : 
	ar := Model.Area{}
	InsertArea(10, 10, []string{"persegi","segita","persegi panjang"}, &ar)
 ```

# Soal Ketiga
```
    //Pusedo Code
	var tempValue = deretKedua
	for i := 0; i < loop; i++ {
		if i == 0 {
			fmt.Print(strconv.Itoa(deretPertama) + ",")
			continue
		}else if i == 1 {
			fmt.Print(strconv.Itoa(deretKedua) + ",")
			continue
		}
		tempValue += deretKedua - deretPertama
		if i + 1 == loop{
			fmt.Print(strconv.Itoa(tempValue))
			continue
		}
		fmt.Print(strconv.Itoa(tempValue) + ",")
	}
```
Diagram

![alt text](https://ik.imagekit.io/f0f706sv1/Screenshot_48_RpnMUvL4Y.png?ik-sdk-version=javascript-1.4.3&updatedAt=1657975907674)

# Soal Keempat
Pesudo Code

```
func main() {
	array:= []float64{4, -7, -5, 3, 3.3, 9, 0, 10, 0.2};
	fmt.Println(AscendingSort(array)) // [-7 -5 0 0.2 3 3.3 4 9 10]
	fmt.Println(DescendingSort(array)) // [10 9 4 3.3 3 0.2 0 -5 -7]

}

func AscendingSort(array[] float64)[]float64 {
	for i:=0; i< len(array)-1; i++ {
		for j:=0; j < len(array)-i-1; j++ {
			if (array[j] > array[j+1]) {
				array[j], array[j+1] = array[j+1], array[j]
			}
		}
	}
	return array
}

func DescendingSort(array[] float64)[]float64 {
	for i:=0; i< len(array)-1; i++ {
		for j:=0; j < len(array)-i-1; j++ {
			if (array[j] < array[j+1]) {
				array[j], array[j+1] = array[j+1], array[j]
			}
		}
	}
	return array
}
```

Diagram

![alt text](https://ik.imagekit.io/f0f706sv1/Screenshot_49_zRoZgAysh.png?ik-sdk-version=javascript-1.4.3&updatedAt=1658022532747)
