package dto

import "errors"

type RequestUserLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (input *RequestUserLogin) ValidateLoginUser() (err error) {

	if input.Username == "" || input.Password == "" {
		errors.New("username atau password tidak boleh kosong")
		return
	}

	return
}