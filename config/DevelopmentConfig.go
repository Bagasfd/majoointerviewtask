package config

import (
	"fmt"
	"github.com/tkanos/gonfig"
	"os"
)

var ApplicationConfiguration DevelopmentConfig

type DevelopmentConfig struct {
	Server struct {
		Host      string `json:"host"`
		Port      string `json:"port"`
		ServiceID string `json:"service_id"`
	} `json:"server"`
	Postgresql struct {
		Address           string `json:"address"`
		DefaultSchema     string `json:"default_schema"`
		MaxOpenConnection int    `json:"max_open_connection"`
		MaxIdleConnection int    `json:"max_idle_connection"`
	} `json:"postgresql"`
	JWT struct {
		Key string `json:"key"`
	} `json:"jwt"`
}

func (input DevelopmentConfig) GetJWTKey() string {
	return input.JWT.Key
}

func (input DevelopmentConfig) GetServerHost() string {
	return input.Server.Host
}
func (input DevelopmentConfig) GetServerPort() string {
	return input.Server.Port
}

func (input DevelopmentConfig) GetPostgreSQLAddress() string {
	return input.Postgresql.Address
}
func (input DevelopmentConfig) GetPostgreSQLDefaultSchema() string {
	return input.Postgresql.DefaultSchema
}
func (input DevelopmentConfig) GetPostgreSQLMaxOpenConnection() int {
	return input.Postgresql.MaxOpenConnection
}
func (input DevelopmentConfig) GetPostgreSQLMaxIdleConnection() int {
	return input.Postgresql.MaxIdleConnection
}

func GenerateConfiguration(configPath string) {
	var err error

	temp := DevelopmentConfig{}
	err = gonfig.GetConf(configPath+"/config-development.json", &temp)
	ApplicationConfiguration = temp

	if err != nil {
		fmt.Print(err)
		os.Exit(2)
	}
}
