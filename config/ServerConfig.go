package config

import (
	"MajooTaskInterview/database"
	"database/sql"
)

var ServerAttribute serverAttribute

type serverAttribute struct {
	DBConnection                 *sql.DB
}

func SetServerAttribute() {
	dbParam := ApplicationConfiguration.GetPostgreSQLDefaultSchema()
	dbConnection := ApplicationConfiguration.GetPostgreSQLAddress()
	dbMaxOpenConnection := ApplicationConfiguration.GetPostgreSQLMaxOpenConnection()
	dbMaxIdleConnection := ApplicationConfiguration.GetPostgreSQLMaxIdleConnection()
	ServerAttribute.DBConnection = database.GetDbConnection(dbParam, dbConnection, dbMaxOpenConnection, dbMaxIdleConnection)
}